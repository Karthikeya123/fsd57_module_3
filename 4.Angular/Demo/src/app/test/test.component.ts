import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit{

  id: number;
  name: string;
  avg: number;

  address:any;
  hobbies:any;

  constructor(){
  //alert("constructor invoked...");

    this.id = 101;
    this.name = "Ramu";
    this.avg = 56.54;

    this.address = {
      streetNo: 594,
      city: 'Hyderabad',
      state: 'Telangana'
    };

    this.hobbies = ['Sleeping', 'Eating', 'Playing','Running','Swimming'];
  }

  ngOnInit() {
  //alert("ngOnInit invoked...");
  }

}
