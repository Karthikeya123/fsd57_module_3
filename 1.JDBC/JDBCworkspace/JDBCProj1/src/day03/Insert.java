package day03;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class Insert {
	
	public static void insertRecordPrepareStatement() {
		PreparedStatement pstmt = null;
		Connection con = null;
		con = DbConnection.getConnection();
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Employee Id, Name, Salary, gender, email,password");
		int empId = sc.nextInt();
		sc.nextLine();
		String empName = sc.nextLine();
		double salary = sc.nextDouble();
		String gender = sc.next(), email = sc.next(), password = sc.next();
		
		String query = "insert into Employee values(?,?,?,?,?,?)";
	    
		
		try {
			con = DbConnection.getConnection();
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, empId);
			pstmt.setString(2, empName);
			pstmt.setDouble(3, salary);
			pstmt.setString(4, gender);
			pstmt.setString(5, email);
			pstmt.setString(6, password);
			int cnt = pstmt.executeUpdate();
			if(cnt > 0) {
				System.out.println("Employee Record inserted successfully !!");
				System.out.println("-----------------------------------------------------");
				Fetching_Details.printAllEmployees();
			} else {
				System.out.println("Employee Record not inserted successfully !!");
			}
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			sc.close();
		}
	}
	
	public static void main(String[] args) {
		//insertRecord();
		insertRecordPrepareStatement() ;
	}
}